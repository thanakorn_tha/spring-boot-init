package com.thanakorn.init.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by thanakorn.tha on 19/11/2018 AD.
 */
@RestController
public class helloController {

    @GetMapping(value = "test")
    public String getNameOfService() {
        return "POD service";
    }
}
